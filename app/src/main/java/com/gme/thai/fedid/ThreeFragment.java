package com.gme.thai.fedid;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.BottomNavigationView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;
import com.gme.thai.fedid.dialog.DialogFabMenu;
import android.view.View.OnClickListener;

import java.util.HashMap;

public class ThreeFragment extends Fragment {


    SessionManager session;




    RelativeLayout rel1;

    private String TAG = getClass().getSimpleName();


    public ThreeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
    }
    private TextView mTextMessage;
    Activity context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getActivity();

        final View view = inflater.inflate(R.layout.fragment_three, container, false);



 return view ;

  //  @Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      //  getMenuInflater().inflate(R.menu.menu_main, menu);
       // return true;
   // }


    }
    public void onStart(){
        super.onStart();



        session = new SessionManager(this.getContext());
        //session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String email = user.get(SessionManager.KEY_EMAIL);
        String lg = user.get(SessionManager.KEY_lg);

        Button bt4 =(Button)context.findViewById(R.id.button6);
        ImageButton bt1=(ImageButton)context.findViewById(R.id.imageButton3);

        if(lg==null || lg.equals("en")){
            bt1.setImageResource(R.drawable.mpro);

        }
        else{
            bt1.setImageResource(R.drawable.mprocn);
        }

        ImageButton bt2=(ImageButton)context.findViewById(R.id.imageButton7);
        if(lg==null || lg.equals("en")){
            bt2.setImageResource(R.drawable.mpartner);

        }
        else{
            bt2.setImageResource(R.drawable.mpartnercn);
        }


        ImageButton bt3=(ImageButton)context.findViewById(R.id.imageButton8);
        TextView textView = (TextView)context.findViewById(R.id.textView3);
        if(lg==null || lg.equals("en")){
            bt3.setImageResource(R.drawable.fpartner);

        }
        else{
            bt3.setImageResource(R.drawable.fpartnercn);
        }




        if (email == null || email.length() == 0) {
            bt1.setVisibility(View.GONE);
            bt2.setVisibility(View.GONE);
            bt3.setVisibility(View.GONE);
            textView.setText("Please login to view Partners feature.");

            bt4.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                    //create an Intent object
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    //add data to the Intent object
                    // intent.putExtra("text", et.getText().toString());

                    startActivity(intent);
                }
            });
        }


        else {
            textView.setVisibility(View.GONE);

            bt4.setVisibility(View.GONE);
            bt3.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                    //create an Intent object
                    Intent intent = new Intent(getContext(), FindpartnerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    //add data to the Intent object
                    // intent.putExtra("text", et.getText().toString());

                    startActivity(intent);
                }
            });


            bt2.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                    //create an Intent object
                    Intent intent = new Intent(getContext(), MyPartnerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    //add data to the Intent object
                    //intent.putExtra("text", et.getText().toString());
                    //start the second activity
                    startActivity(intent);
                }
            });


            bt1.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                    //create an Intent object
                    Intent intent = new Intent(getContext(), UpdateMyProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    //add data to the Intent object
                    //intent.putExtra("text", et.getText().toString());
                    //start the second activity
                    startActivity(intent);
                }
            });


        }

    }

}