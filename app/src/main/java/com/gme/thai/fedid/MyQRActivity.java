package com.gme.thai.fedid;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.HashMap;

public class MyQRActivity extends LocalizationActivity {

    ImageView imageView;
    Button button;
    EditText editText;
    String EditTextValue ;
    Thread thread ;
    public final static int QRcodeWidth = 500 ;
    Bitmap bitmap ;
    SessionManager session;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    ImageView gambarBarcode;

TextView textView;
    TextView textView1;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qr);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(getResources().getString(R.string.myqrcode));
        textView = (TextView)findViewById(R.id.textView9);
        textView1= (TextView)findViewById(R.id.textView15);
        session = new SessionManager(this.getApplicationContext());
       // session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String email = user.get(SessionManager.KEY_EMAIL);
        Button bt1=(Button) findViewById(R.id.button5);

        if (email == null || email.length() == 0) {

            textView.setText("Please login to generate My QR Code ");





        }else{
            textView1.setText(getResources().getString(R.string.myqrcodetext));
            bt1.setVisibility(View.GONE);
            textView.setText(getResources().getString(R.string.email)+": "+email.toString());
            imageView = (ImageView)findViewById(R.id.imageView10);
            //editText = (EditText)findViewById(R.id.editText);
            // button = (Button)findViewById(R.id.button);

            EditTextValue = "http://202.76.236.10/smehk/template3/cn/app/supplier-list-detail-scan-fed.asp?email=" + email;
            try {
                bitmap = TextToImageEncode(EditTextValue);

                imageView.setImageBitmap(bitmap);

            } catch (WriterException e) {
                e.printStackTrace();
            }


        }



       // String password = user.get(SessionManager.KEY_PASSWORD);
      //  String memberID = user.get(SessionManager.KEY_MemberID);
       // String name = user.get(SessionManager.KEY_Name);
      //  String contact = user.get(SessionManager.KEY_CONTACT);
       // String Position = user.get(SessionManager.KEY_POSTION);







    }


    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor):getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }


    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    public void onStart(){
        super.onStart();
        Button bt1=(Button) findViewById(R.id.button5);

        bt1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                Intent intent = new Intent(MyQRActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });


    }




}
