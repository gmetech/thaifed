package com.gme.thai.fedid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventTicketActivity extends AppCompatActivity {
    private List<Eventticket> eventlist = new ArrayList<>();

    private RecyclerView recyclerView;
    private EventticketAdapter mAdapter;
    SessionManager session;
    private static final String TAG = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Event Ticket");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new EventticketAdapter(eventlist);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Eventticket movie = eventlist.get(position);
                //Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(EventTicketActivity.this, EventDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareMovieData();




    }
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void prepareMovieData() {

        session = new SessionManager(this.getApplicationContext());
        //session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String email = user.get(SessionManager.KEY_EMAIL);
        String payment = user.get(SessionManager.KEY_Payment);
        TextView textView =(TextView) findViewById(R.id.textView);
        Button bt1=(Button) findViewById(R.id.button3);
        //Log.d(TAG,payment);
       if (email == null || email.length() == 0) {

            textView.setText("Please Login to view your tickets.");
        }
        else if(payment.trim().equals("Pending") ){


            textView.setText("You dont have any ticket.");
           bt1.setVisibility(View.GONE);
        }
        else if(payment.trim().equals("Paid") ) {
            bt1.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);

            Eventticket eventticket = new Eventticket("Belt & Road : Match & Go", "18 April 2018 HKFCSME, Hong Kong", "Status: Expired");
            eventlist.add(eventticket);


           // Eventticket eventticket = new Eventticket("Please login to view the tickets.", "18 April 2018 HKFCSME, Hong Kong", "Status: Active");
            //eventlist.add(eventticket);


        }


        //eventticket  = new Eventticket("Inside Out", "Animation, Kids & Family", "2015");
       // eventlist.add(eventticket);




        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }


    public void onStart(){
        super.onStart();
        Button bt1=(Button) findViewById(R.id.button3);

        bt1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                //EditText et= (EditText)context.findViewById(R.id.txt_edit);
                Intent intent = new Intent(EventTicketActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });


    }

}
