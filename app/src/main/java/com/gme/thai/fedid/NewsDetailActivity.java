package com.gme.thai.fedid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.gme.thai.fedid.entity.NewsInfo;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsDetailActivity extends AppCompatActivity {

    @BindView(R.id.ivback)
    ImageView ivback;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tvDes)
    TextView tvDes;
    @BindView(R.id.ivImage)
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        NewsInfo.RecordsBean detail = (NewsInfo.RecordsBean) getIntent().getSerializableExtra("detail");
        if (detail != null) {
            tvTitle.setText(detail.getTitle());
            tvDes.setText(detail.getDescription());
            tvTime.setText(detail.getCreatedt());
            Picasso.with(this).load(detail.getImg()).into(ivImage);
        }

    }

    @OnClick(R.id.ivback)
    public void onViewClicked() {
        finish();
    }
}
