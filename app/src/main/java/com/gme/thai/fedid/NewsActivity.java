package com.gme.thai.fedid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.loadmore.SimpleLoadMoreView;
import com.gme.thai.fedid.adapter.NewsAdapter;
import com.gme.thai.fedid.entity.NewsInfo;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsActivity extends AppCompatActivity {


    @BindView(R.id.ivback)
    ImageView ivback;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh_layout)
    VerticalSwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news2);
        ButterKnife.bind(this);
        init();
        loadData();
    }

    public void loadData() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "http://202.76.236.10/smehk/template3/cn/api/api-news.asp",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        swipeRefreshLayout.setRefreshing(false);
                        NewsInfo newsInfo = new Gson().fromJson(response.toString(), NewsInfo.class);

                        if (newsInfo.getData() != null) {
                            if (newsInfo.getData().getRecords() != null) {
                                mlist.clear();
                                mlist.addAll(newsInfo.getData().getRecords());
                                adapter.notifyDataSetChanged();
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }

    NewsAdapter adapter;

    List<NewsInfo.RecordsBean> mlist = new ArrayList<>();

    public void init() {
        adapter = new NewsAdapter(mlist);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setLoadMoreView(new SimpleLoadMoreView());
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(new Intent(NewsActivity.this, NewsDetailActivity.class)
                        .putExtra("detail", mlist.get(position)));
            }
        });
//        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                Toast.makeText(NewsActivity.this, "yyy", Toast.LENGTH_SHORT).show();
//            }
//        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
    }

    @OnClick(R.id.ivback)
    public void onViewClicked() {
        finish();
    }
}
