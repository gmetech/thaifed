package com.gme.thai.fedid.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gme.thai.fedid.R;
import com.gme.thai.fedid.entity.NewsInfo;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by xuchichi on 2018/11/14.
 */

public class NewsAdapter extends BaseQuickAdapter<NewsInfo.RecordsBean, BaseViewHolder> {

    public NewsAdapter(@Nullable List<NewsInfo.RecordsBean> data) {
        super(R.layout.item_news, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewsInfo.RecordsBean item) {
        TextView tvTitle = helper.getView(R.id.tvTitle);
        TextView tvTime = helper.getView(R.id.tvTime);
        tvTitle.setText(item.getTitle());
        tvTime.setText(item.getCreatedt());
        Picasso.with(mContext).load(item.getImg()).into((ImageView) helper.getView(R.id.image));

    }
}
