package com.gme.thai.fedid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;
	
	// Editor for Shared preferences
	Editor editor;
	
	// Context
	Context _context;
	
	// Shared pref mode
	int PRIVATE_MODE = 0;
	
	// Sharedpref file name
	private static final String PREF_NAME = "AndroidHivePref";
	
	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	
	// User name (make variable public to access from outside)
	public static final String KEY_EMAIL = "email";
	
	// Email address (make variable public to access from outside)
	public static final String KEY_PASSWORD= "password";
	public static final String KEY_MemberID= "memberId";
	public static final String KEY_POSTION= "position";
	public static final String KEY_CONTACT= "contact";
	public static final String KEY_Name= "name";
	public static final String KEY_Payment= "payment";
	public static final String KEY_lg= "lg";
	public static final String KEY_country= "country";
	public static final String KEY_push= "push";

	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	/**
	 * Create login session
	 * */
	public void createLoginSession(String email, String password, String memberID, String name, String position, String contact, String payment ){
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		// Storing email in pref
		editor.putString(KEY_EMAIL, email);

		// Storing name in pref
		editor.putString(KEY_PASSWORD, password);
		editor.putString(KEY_MemberID, memberID);
		editor.putString(KEY_Name, name);

		editor.putString(KEY_POSTION, position);
		editor.putString(KEY_CONTACT, contact);
		editor.putString(KEY_Payment, payment);

		// commit changes
		editor.commit();
	}	




	public  void settingsessionlg(String lg){
		editor.putString(KEY_lg, lg);
		editor.commit();

	}
	public  void settingsessioncountry(String country){
		editor.putString(KEY_country, country);
		editor.commit();

	}

	public  void settingsessionpush(String push){
		editor.putString(KEY_push, push);
		editor.commit();

	}


	/**
	 * Check login method wil check user login status
	 * If false it will redirect user to login page
	 * Else won't do anything
	 * */
	public void checkLogin(){
		// Check login status


		if(!this.isLoggedIn()){


			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			// Staring Login Activity
			_context.startActivity(i);
		}
		
	}
	
	
	
	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// user name
		user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
		
		// user email id
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
		user.put(KEY_MemberID, pref.getString(KEY_MemberID, null));
		user.put(KEY_Name, pref.getString(KEY_Name, null));

		user.put(KEY_POSTION, pref.getString(KEY_POSTION, null));
		user.put(KEY_CONTACT, pref.getString(KEY_CONTACT, null));
		user.put(KEY_Payment, pref.getString(KEY_Payment, null));
		user.put(KEY_lg, pref.getString(KEY_lg, null));
		user.put(KEY_country, pref.getString(KEY_country, null));
		user.put(KEY_push, pref.getString(KEY_push, null));
		// return user
		return user;
	}
	
	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
		
		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, MainActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		// Staring Login Activity
		_context.startActivity(i);
	}
	
	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}
}
