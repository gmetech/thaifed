package com.gme.thai.fedid;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Movie {
    private String title;
    private String imgurl;


    public Movie() {
    }

    public Movie(String title, String imgurl) {
        this.title = title;
        this.imgurl = imgurl;
        //this.year = year;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getImgurl() {
       return imgurl;
  }
    public void setimgurl(String imgurl) {
       this.imgurl = imgurl;
    }

   // public String getGenre() {
       // return genre;
   // }

    //public void setGenre(String genre) {
      //  this.genre = genre;
   // }
}
