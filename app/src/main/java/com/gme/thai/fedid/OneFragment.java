package com.gme.thai.fedid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gme.thai.fedid.entity.ImageInfo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;


public class OneFragment extends Fragment {
    private BannerSlider homeBanner, eventbanner, onlinebanner, smebanner, productbanner;
    SessionManager session;
    VerticalSwipeRefreshLayout swipeRefreshLayout;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        session = new SessionManager(this.getContext());


        View view = inflater.inflate(R.layout.fragment_one, container, false);


        homeBanner = view.findViewById(R.id.homeBanner);
        eventbanner = view.findViewById(R.id.eventbanner);
        onlinebanner = view.findViewById(R.id.onlinebanner);
        smebanner = view.findViewById(R.id.smebanner);
        productbanner = view.findViewById(R.id.productbanner);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.blue));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loadData();
            }
        });


        requestQueue = Volley.newRequestQueue(getActivity());
        loadData();
        return view;

    }

    public List<String> homeImgUrlist = new ArrayList<>();
    public List<Banner> homeImgList = new ArrayList<>();

    public List<String> productImgUrlist = new ArrayList<>();
    public List<Banner> productImgList = new ArrayList<>();

    public List<String> smeImgUrlist = new ArrayList<>();
    public List<Banner> smeImgList = new ArrayList<>();

    public List<String> onlineImgUrlist = new ArrayList<>();
    public List<Banner> onlineImgList = new ArrayList<>();

    public List<String> eventImgUrlist = new ArrayList<>();
    public List<Banner> eventImgList = new ArrayList<>();
    RequestQueue requestQueue;

    public void loadData() {
        String url = "http://202.76.236.10/smehk/template3/cn/api/api-home.asp?id=0001&lg=en";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                swipeRefreshLayout.setRefreshing(false);


                homeBanner.removeAllBanners();
                onlinebanner.removeAllBanners();
                eventbanner.removeAllBanners();
                smebanner.removeAllBanners();
                productbanner.removeAllBanners();


                homeImgUrlist.clear();
                homeImgList.clear();

                eventImgUrlist.clear();
                eventImgList.clear();

                onlineImgUrlist.clear();
                onlineImgList.clear();

                smeImgUrlist.clear();
                smeImgList.clear();

                productImgUrlist.clear();
                productImgList.clear();

                ImageInfo imageInfo = new Gson().fromJson(response, ImageInfo.class);
                if (imageInfo != null) {
                    if (imageInfo.records.homebanner != null) {
                        for (int i = 0; i < imageInfo.records.homebanner.size(); i++) {
                            homeImgUrlist.add(imageInfo.records.homebanner.get(i).bannerlink);
                            homeImgList.add(new RemoteBanner(imageInfo.records.homebanner.get(i).bannerimg));
                        }

                        addBanners(homeImgList, homeImgUrlist, homeBanner);
                    }
                    if (imageInfo.records.eventbanner != null) {

                        for (int i = 0; i < imageInfo.records.eventbanner.size(); i++) {
                            eventImgUrlist.add(imageInfo.records.eventbanner.get(i).bannerlink);
                            eventImgList.add(new RemoteBanner(imageInfo.records.eventbanner.get(i).bannerimg));
                        }
                        addBanners(eventImgList, eventImgUrlist, eventbanner);
                    }
                    if (imageInfo.records.onlinebanner != null) {

                        for (int i = 0; i < imageInfo.records.onlinebanner.size(); i++) {
                            onlineImgUrlist.add(imageInfo.records.onlinebanner.get(i).bannerlink);
                            onlineImgList.add(new RemoteBanner(imageInfo.records.onlinebanner.get(i).bannerimg));
                        }
                        addBanners(onlineImgList, onlineImgUrlist, onlinebanner);

                    }
                    if (imageInfo.records.smebanner != null) {


                        for (int i = 0; i < imageInfo.records.smebanner.size(); i++) {
                            smeImgUrlist.add(imageInfo.records.smebanner.get(i).bannerlink);
                            smeImgList.add(new RemoteBanner(imageInfo.records.smebanner.get(i).bannerimg));
                        }
                        addBanners(smeImgList, smeImgUrlist, smebanner);

                    }
                    if (imageInfo.records.productbanner != null) {
                        for (int i = 0; i < imageInfo.records.productbanner.size(); i++) {
                            productImgUrlist.add(imageInfo.records.productbanner.get(i).bannerlink);
                            productImgList.add(new RemoteBanner(imageInfo.records.productbanner.get(i).bannerimg))
                            ;
                        }
                        addBanners(productImgList, productImgUrlist, productbanner);
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);

                Log.e("OneFragment", "error:" + error.getMessage());

            }
        });

        requestQueue.add(request);
    }

    private void addBanners(List<Banner> imgList, List<String> linkList, BannerSlider bannerSlider) {
        bannerSlider.setBanners(imgList);

        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {

                startActivity(new Intent(getContext(), WebviewActivity.class)
                        .putExtra("url", linkList.get(position)));

            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        requestQueue.cancelAll(this);
    }
}
