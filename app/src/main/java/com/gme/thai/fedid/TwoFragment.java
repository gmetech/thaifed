package com.gme.thai.fedid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TwoFragment extends Fragment{
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Context c = getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.fragment_two, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Movie movie = movieList.get(position);
                //Toast.makeText(getActivity(), movie.getTitle() + String.valueOf(position), Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(getContext(), SmeListActivity.class);
                //add data to the Intent object
               // intent.putExtra("id", "test");
                //start the second activity
             //   startActivity(intent);

               // intent.putExtra("test","Nisar");
               // getActivity().startActivity(intent);

              //  Intent intent = new Intent(reshte.this,List1.class);
                Bundle bundle = new Bundle();
                bundle.putString("key1",String.valueOf(position));

                intent.putExtras(bundle);
                getActivity().startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareMovieData();


        // Inflate the layout for this fragment
        return view ;
    }


    private void prepareMovieData() {
         Movie movie =



        movie = new Movie(getResources().getString(R.string.category6),"http://202.76.236.10/smehk/icon/FoodProducts.png");
        movieList.add(movie);


        movie = new Movie(getResources().getString(R.string.category16),"http://202.76.236.10/smehk/icon/Hospital.png");
        movieList.add(movie);



        movie = new Movie(getResources().getString(R.string.category18),"http://202.76.236.10/smehk/icon/RubberProducts.png");
        movieList.add(movie);



        movie = new Movie(getResources().getString(R.string.category21),"http://202.76.236.10/smehk/icon/Tools.png");
        movieList.add(movie);



        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }


}

