package com.gme.thai.fedid;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import java.util.HashMap;

public class SettingActivity extends LocalizationActivity {
    RadioButton zh, en, ch, my, in, th;
    RadioGroup language, country;
    CheckBox notification;
    SessionManager session;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        session = new SessionManager(getApplicationContext());
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(getResources().getString(R.string.setting));


        zh = (RadioButton) findViewById(R.id.radioButton4);
        en = (RadioButton) findViewById(R.id.radioButton5);

        ch=(RadioButton) findViewById(R.id.radioButton6);
        in=(RadioButton) findViewById(R.id.radioButton7);
        my=(RadioButton) findViewById(R.id.radioButton8);
        th=(RadioButton) findViewById(R.id.radioButton9);

        notification = (CheckBox) findViewById(R.id.checkBoxpush);

        language=(RadioGroup)findViewById(R.id.radioGroupc);
        country=(RadioGroup)findViewById(R.id.radioGroupcc);

        HashMap<String, String> user = session.getUserDetails();



        String lg = user.get(SessionManager.KEY_lg);
        String allcountry = user.get(SessionManager.KEY_country);
        String push = user.get(SessionManager.KEY_push);

        if(lg==null || lg.equals("en")){
            en.setChecked(true);

        }
        else{
            zh.setChecked(true);
        }


        if(allcountry==null || allcountry.equals("my")){
            my.setChecked(true);

        }
        else if(allcountry.equals("ch")){
            ch.setChecked(true);
        }
        else if(allcountry.equals("th")){
            th.setChecked(true);
        }
        else if(allcountry.equals("in")){
            in.setChecked(true);
        }


        if(push==null || push.equals("yes")){
            notification.setChecked(true);

        }
        else{
            notification.setChecked(false);
        }

        //my.setChecked(true);
        //notification.setChecked(true);

        language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {

                int id = language.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radioButton4:
                        Toast.makeText(getApplicationContext(),"zh" , Toast.LENGTH_SHORT).show();
                        session.settingsessionlg("zh");

                        break;
                    case R.id.radioButton5:
                        Toast.makeText(getApplicationContext(),"en" , Toast.LENGTH_SHORT).show();
                        session.settingsessionlg("en");
                       // Intent refresh = new Intent(getApplicationContext(), MainActivity.class);

                        break;

                }
               // RadioButton checkedRadioButton = (RadioButton) findViewById(checkedId);
                //String text = checkedRadioButton.getText().toString();
               // Toast.makeText(getApplicationContext(), , Toast.LENGTH_SHORT).show();
            }
        });



        country.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {

                int id = country.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radioButton6:
                        Toast.makeText(getApplicationContext(),"ch" , Toast.LENGTH_SHORT).show();
                        session.settingsessioncountry("ch");

                        break;
                    case R.id.radioButton7:
                        Toast.makeText(getApplicationContext(),"in" , Toast.LENGTH_SHORT).show();
                        session.settingsessioncountry("in");
                        break;

                    case R.id.radioButton8:
                        Toast.makeText(getApplicationContext(),"my" , Toast.LENGTH_SHORT).show();
                        session.settingsessioncountry("my");
                        // Intent refresh = new Intent(getApplicationContext(), MainActivity.class);

                        break;

                    case R.id.radioButton9:
                        Toast.makeText(getApplicationContext(),"th" , Toast.LENGTH_SHORT).show();
                        session.settingsessioncountry("th");
                        // Intent refresh = new Intent(getApplicationContext(), MainActivity.class);

                        break;



                }
                // RadioButton checkedRadioButton = (RadioButton) findViewById(checkedId);
                //String text = checkedRadioButton.getText().toString();
                // Toast.makeText(getApplicationContext(), , Toast.LENGTH_SHORT).show();
            }
        });


        CheckBox checkBox = ( CheckBox ) findViewById( R.id.checkBoxpush );
        checkBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if ( ((CheckBox)v).isChecked() ) {
                    session.settingsessionpush("yes");
                }
                else{

                    session.settingsessionpush("no");
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settingmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.save) {


            Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(refresh);//Start the same Activity
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
