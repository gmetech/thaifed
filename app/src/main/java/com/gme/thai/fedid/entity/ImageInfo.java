package com.gme.thai.fedid.entity;

import java.io.Serializable;
import java.util.List;

import ss.com.bannerslider.banners.Banner;

/**
 * Created by xuchichi on 2018/10/22.
 */

public class ImageInfo implements Serializable {


    /**
     * records : {"homebanner":[{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"Homeimg","bannerlink":"homelink","createdt":"2018-10-10 10:10:00"}],"productbanner":[{"id":"1","associationid":"0001","lg":"en","status":"active","bannerimg":"product1","bannerlink":"product1 ","createdt":"2018-10-10 10:10:10"}],"smebanner":[{"id":"1","associationid":"0001","lg":"en","status":"active","banner":"banner1","bannerlink":"banner2","createdt":"2018-10-10 10:00:00"}],"onlinebanner":[{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"online1","bannerlink":"Online2","createdt":"2018-10-10 10:00:00"}],"eventbanner":[{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"event1","bannerlink":"event1","createdt":"2018-10-10 10:00:00"}],"lg":"en","associationid":"0001","association":"SME ALLIANCE","status":"Active","createdt":"2018-10-10 10:00:00"}
     */

    public RecordsBean records;


    public static class RecordsBean implements Serializable{
        /**
         * homebanner : [{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"Homeimg","bannerlink":"homelink","createdt":"2018-10-10 10:10:00"}]
         * productbanner : [{"id":"1","associationid":"0001","lg":"en","status":"active","bannerimg":"product1","bannerlink":"product1 ","createdt":"2018-10-10 10:10:10"}]
         * smebanner : [{"id":"1","associationid":"0001","lg":"en","status":"active","banner":"banner1","bannerlink":"banner2","createdt":"2018-10-10 10:00:00"}]
         * onlinebanner : [{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"online1","bannerlink":"Online2","createdt":"2018-10-10 10:00:00"}]
         * eventbanner : [{"id":"1","associationid":"0001","lg":"en","status":"Active","bannerimg":"event1","bannerlink":"event1","createdt":"2018-10-10 10:00:00"}]
         * lg : en
         * associationid : 0001
         * association : SME ALLIANCE
         * status : Active
         * createdt : 2018-10-10 10:00:00
         */

        public String lg;
        public String associationid;
        public String association;
        public String status;
        public String createdt;
        public List<BannerBean> homebanner;
        public List<BannerBean> productbanner;
        public List<BannerBean> smebanner;
        public List<BannerBean> onlinebanner;
        public List<BannerBean> eventbanner;

    }
    public static class BannerBean extends Banner implements Serializable{
        /**
         * id : 1
         * associationid : 0001
         * lg : en
         * status : Active
         * bannerimg : Homeimg
         * bannerlink : homelink
         * createdt : 2018-10-10 10:10:00
         */

//        public String id;
        public String associationid;
        public String lg;
        public String status;
        public String bannerimg;
        public String bannerlink;
        public String createdt;
    }
}
