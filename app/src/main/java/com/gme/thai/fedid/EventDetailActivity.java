package com.gme.thai.fedid;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class EventDetailActivity extends AppCompatActivity {
    ImageView imageView;
    Button button;
    EditText editText;
    String EditTextValue ;
    Thread thread ;
    public final static int QRcodeWidth = 500 ;
    Bitmap bitmap ;
    SessionManager session;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    ImageView gambarBarcode;

    TextView bardcodetext;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Event Ticket Detail");
        session = new SessionManager(this.getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();




        String email = user.get(SessionManager.KEY_EMAIL);
        String password = user.get(SessionManager.KEY_PASSWORD);
        String memberID = user.get(SessionManager.KEY_MemberID);
        String name = user.get(SessionManager.KEY_Name);
        String contact = user.get(SessionManager.KEY_CONTACT);
        String Position = user.get(SessionManager.KEY_POSTION);



        imageView = (ImageView)findViewById(R.id.imageView7);
        //editText = (EditText)findViewById(R.id.editText);
       // button = (Button)findViewById(R.id.button);

        EditTextValue = "http://202.76.236.10/smehk/template3/cn/ticket/verify.asp?no=" + memberID;
        try {
            bitmap = TextToImageEncode(EditTextValue);

            imageView.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        gambarBarcode = (ImageView) findViewById(R.id.imageView8);
        bardcodetext = (TextView) findViewById(R.id.textView);

        TextView textView10 =(TextView) findViewById(R.id.textView10);
        TextView textView11 =(TextView) findViewById(R.id.textView11);
        TextView textView12 =(TextView) findViewById(R.id.textView12);
        TextView textView13 =(TextView) findViewById(R.id.textView13);

        textView10.setText("Name: "+name.toUpperCase());
        textView11.setText("Email: "+email);
        textView12.setText("Positon: "+Position.toUpperCase());
       // textView13.setText("Contact "+contact);
        textView13.setText("Ticket No: "+memberID);

        try {
            bitmap = encodeAsBitmap("HKFCSME"+memberID, BarcodeFormat.CODE_128, 600, 100);
            bardcodetext.setText("HKFCSME"+memberID);
            gambarBarcode.setImageBitmap(bitmap);
           // textnya.setText(textInput.getText().toString());
        } catch (WriterException e) {
            e.printStackTrace();
        }




    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor):getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }


    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }






    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
