package com.gme.thai.fedid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.gme.thai.fedid.supplier.DistributuionFragment;
import com.gme.thai.fedid.supplier.ImportFragment;
import com.gme.thai.fedid.supplier.OthersFragment;
import com.gme.thai.fedid.supplier.PromotionFragment;
import com.gme.thai.fedid.supplier.SalesFragment;


import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.gme.thai.fedid.supplier.SalesFragment;

import java.util.ArrayList;
import java.util.List;

public class SmeListActivity extends LocalizationActivity {

    private WebView webView;
    ProgressDialog mProgressDialog;
    ProgressDialog progressDialog;

    //String id = intent.getStringExtra("id");
  // Intent intent = getActivity().getIntent();
   // final String sender=this.getIntent().getExtras().getString("test");


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    private String HTTPS_URL ="http://202.76.236.10/smehk/template3/cn/app/supplier-list.asp?id=";
    //private String HTTP_URL ="http://example.com";

    private boolean isSSLErrorDialogShown = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_sme_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(getResources().getString(R.string.supplierslisting));


        // bundle.putString("key1", "1");


        //Toast.makeText(getApplicationContext(),value1,Toast.LENGTH_SHORT).show();


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new DistributuionFragment(), "Distribution");
        adapter.addFrag(new ImportFragment(), "Imports");
        adapter.addFrag(new PromotionFragment(), "Promotion");
        adapter.addFrag(new SalesFragment(), "Sales");
        adapter.addFrag(new OthersFragment(), "Others");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {


            Bundle bundle = getIntent().getExtras();
            String value11 = bundle.getString("key1");
            switch (position) {
                case 0:
                    // Display the main fragment


                    // Display second fragment
                    Fragment DistributuionFragment = new DistributuionFragment();
                    // Pass extra values using bundle extras as arguments.
                    Bundle args = new Bundle();
                    args.putString("EXTRAS", value11);
                    DistributuionFragment.setArguments(args);
                    return DistributuionFragment;


                case 1:


                    // Display second fragment
                    Fragment ImportFragment = new ImportFragment();
                    // Pass extra values using bundle extras as arguments.
                    Bundle args1 = new Bundle();
                    args1.putString("EXTRAS", value11);
                    ImportFragment.setArguments(args1);
                    return ImportFragment;




                case 2:
                    // Display second fragment
                    Fragment PromotionFragment = new PromotionFragment();

                    Bundle args2 = new Bundle();
                    args2.putString("EXTRAS", value11);
                    PromotionFragment.setArguments(args2);

                    return PromotionFragment;



                case 3:
                    // Display second fragment
                    Fragment SalesFragment = new SalesFragment();

                    Bundle args3 = new Bundle();
                    args3.putString("EXTRAS", value11);
                    SalesFragment.setArguments(args3);

                    return SalesFragment;



                case 4:
                    // Display second fragment
                    Fragment OthersFragment = new OthersFragment();

                    Bundle args4 = new Bundle();
                    args4.putString("EXTRAS", value11);
                    OthersFragment.setArguments(args4);

                    return OthersFragment;

                default:
                    return new DistributuionFragment();
            }





           /// return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
