package com.gme.thai.fedid.entity;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by xuchichi on 2018/8/31.
 */

public class BaseEntity implements Serializable {

    public int status;
    public String Message;
    public String error_message;

    public String getMessage() {
        if (!TextUtils.isEmpty(Message)){
            return Message;
        }else{
            return error_message;
        }

    }

    public boolean isSuccess() {
        if (status==200) {
            return true;
        }
        return false;

    }
}
