package com.gme.thai.fedid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.thejuki.kformmaster.helper.AutoCompleteBuilder;
import com.thejuki.kformmaster.helper.AutoCompleteTokenBuilder;
import com.thejuki.kformmaster.helper.ButtonBuilder;
import com.thejuki.kformmaster.helper.CheckBoxBuilder;
import com.thejuki.kformmaster.helper.DateBuilder;
import com.thejuki.kformmaster.helper.DateTimeBuilder;
import com.thejuki.kformmaster.helper.DropDownBuilder;
import com.thejuki.kformmaster.helper.EmailEditTextBuilder;
import com.thejuki.kformmaster.helper.FormBuildHelper;
import com.thejuki.kformmaster.helper.HeaderBuilder;
import com.thejuki.kformmaster.helper.MultiCheckBoxBuilder;
import com.thejuki.kformmaster.helper.MultiLineEditTextBuilder;
import com.thejuki.kformmaster.helper.NumberEditTextBuilder;
import com.thejuki.kformmaster.helper.PasswordEditTextBuilder;
import com.thejuki.kformmaster.helper.PhoneEditTextBuilder;
import com.thejuki.kformmaster.helper.SingleLineEditTextBuilder;
import com.thejuki.kformmaster.helper.SliderBuilder;
import com.thejuki.kformmaster.helper.SwitchBuilder;
import com.thejuki.kformmaster.helper.TextViewBuilder;
import com.thejuki.kformmaster.helper.TimeBuilder;
import com.thejuki.kformmaster.listener.OnFormElementValueChangedListener;
import com.thejuki.kformmaster.model.BaseFormElement;
import com.thejuki.kformmaster.model.FormPickerDateElement;
import com.thejuki.kformmasterexample.adapter.ContactAutoCompleteAdapter;
import com.thejuki.kformmasterexample.adapter.EmailAutoCompleteAdapter;
import com.thejuki.kformmasterexample.item.ContactItem;
import com.thejuki.kformmasterexample.item.ListItem;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kotlin.Unit;

/**
 * Form Listener Java Activity
 * <p>
 * Java version of FormListenerActivity
 * OnFormElementValueChangedListener is overridden at the activity level
 * </p>
 *
 * @author <strong>TheJuki</strong> <a href="https://github.com/TheJuki">GitHub</a>
 * @version 1.0
 */
public class FormListenerJavaActivity extends AppCompatActivity implements OnFormElementValueChangedListener {
    private FormBuildHelper formBuilder = null;

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String URL = "http://hkfcsme.org/api/smecompany-email.asp";
    SessionManager session;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Scan Result Profile");

        session = new SessionManager(this.getApplicationContext());


        // String url = "http://202.76.236.10/jcards/cityfriends-api/merchant-api.asp";



        setupForm();
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private List<ListItem> fruits = Arrays.asList(new ListItem(1L, "Banana"),
            new ListItem(2L, "Orange"),
            new ListItem(3L, "Mango"),
            new ListItem(4L, "Guava")
    );

    private enum Tag {
        Email,
        Phone,
        Location,
        Address,
        ZipCode,
        Date,
        Time,
        DateTime,
        Password,
        SingleItem,
        MultiItems,
        AutoCompleteElement,
        AutoCompleteTokenElement,
        ButtonElement,
        TextViewElement,
        SwitchElement,
        SliderElement,
        CheckBoxElement,
    }

    private void setupForm() {
        formBuilder = new FormBuildHelper(this, this, findViewById(R.id.recyclerView), true);

        List<BaseFormElement<?>> elements = new ArrayList<>();

        addEditTexts(elements);

        addDateAndTime(elements);

      //  addPickers(elements);

        //addAutoComplete(elements);

      //  addMarkComplete(elements);

        formBuilder.addFormElements(elements);
        formBuilder.refreshView();
    }

    private void addEditTexts(List<BaseFormElement<?>> elements) {
        elements.add(new HeaderBuilder(getString(R.string.PersonalInfo)).build());

        //EmailEditTextBuilder email = new EmailEditTextBuilder(Tag.Email.ordinal());

        TextViewBuilder name = new TextViewBuilder(Tag.TextViewElement.ordinal());
        name.setTitle(getString(R.string.name));
        name.setValue("Michael Teh");

        //email.setHint(getString(R.string.email_hint));
        elements.add(name.build());

        TextViewBuilder email = new TextViewBuilder(Tag.TextViewElement.ordinal());
        email.setTitle(getString(R.string.email));
        email.setValue("Michael@gmail.com");
        elements.add(email.build());

        TextViewBuilder phone = new TextViewBuilder(Tag.TextViewElement.ordinal());
        phone.setTitle(getString(R.string.Phone));
        phone.setValue("(+60) 0168888888");
        elements.add(phone.build());

        elements.add(new HeaderBuilder(getString(R.string.companyinfo)).build());

        MultiLineEditTextBuilder CompanyName = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        CompanyName.setTitle(getString(R.string.CompanyName));
        CompanyName.setValue("GME technical services sdn bhd");
        elements.add(CompanyName.build());

        TextViewBuilder country = new TextViewBuilder(Tag.ZipCode.ordinal());
        country.setTitle(getString(R.string.Country));
        country.setValue("Malaysia");
        elements.add(country.build());


        MultiLineEditTextBuilder textArea = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        textArea.setTitle(getString(R.string.CAddress));
        textArea.setValue("Level 7, Tower 7, Avenue 3,, The Horizon Phase 1, Bangsar South,, No. 8, Jalan Kerinchi, 59200 Kuala Lumpur, Federal Territory of Kuala Lumpur");
        elements.add(textArea.build());

        TextViewBuilder cperson = new TextViewBuilder(Tag.TextViewElement.ordinal());
        cperson.setTitle(getString(R.string.Cperson));
        cperson.setValue("Michael Teh");
        elements.add(cperson.build());


        TextViewBuilder position = new TextViewBuilder(Tag.TextViewElement.ordinal());
        cperson.setTitle(getString(R.string.Position));
        cperson.setValue("Founder & CEO");
        elements.add(cperson.build());


        TextViewBuilder ctel = new TextViewBuilder(Tag.TextViewElement.ordinal());
        ctel.setTitle(getString(R.string.Ctel));
        ctel.setValue("(+60) 312345678");
        elements.add(ctel.build());


        TextViewBuilder cemail = new TextViewBuilder(Tag.TextViewElement.ordinal());
        cemail.setTitle(getString(R.string.email));
        cemail.setValue("Info@gme.com");
        elements.add(cemail.build());


    }

    private void addDateAndTime(List<BaseFormElement<?>> elements) {
        elements.add(new HeaderBuilder(getString(R.string.Basicinfo)).build());


        MultiLineEditTextBuilder business = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        business.setTitle(getString(R.string.Business));
        business.setValue("Computer - Software, Hardware, Accessories & Services");
        elements.add(business.build());

        MultiLineEditTextBuilder main = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        main.setTitle(getString(R.string.Products));
        main.setValue("Payment Gateway, System Developemnt & Service Provider");
        elements.add(main.build());

        MultiLineEditTextBuilder Tcountry = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        Tcountry.setTitle("Target Country");
        Tcountry.setValue("China, Hong Kong, Malaysia, Vietnam,Thailand, Indonesia and Japan");
        elements.add(Tcountry.build());

        MultiLineEditTextBuilder language = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        language.setTitle("Languages");
        language.setValue("English, Mandarin, Malay, Cantonese, Hakka and Spanish ");
        elements.add(language.build());

        MultiLineEditTextBuilder revenue = new MultiLineEditTextBuilder(Tag.TextViewElement.ordinal());
        revenue.setTitle(getString(R.string.Revenue));
        revenue.setValue("US$100 Million - US$500 Million");
        elements.add(revenue.build());

        TextViewBuilder employees = new TextViewBuilder(Tag.TextViewElement.ordinal());
        employees.setTitle(getString(R.string.Employees));
        employees.setValue("50 - 100");
        elements.add(employees.build());


        TextViewBuilder year = new TextViewBuilder(Tag.TextViewElement.ordinal());
        year.setTitle(getString(R.string.Year));
        year.setValue("1997");
        elements.add(year.build());





       // DateBuilder date = new DateBuilder(Tag.TextViewElement.ordinal());
        //date.setTitle(getString(R.string.Date));
       // date.setDateValue(new Date());
       // date.setDateFormat(new SimpleDateFormat("MM/dd/yyyy", Locale.US));
      //  elements.add(date.build());

      //  TimeBuilder time = new TimeBuilder(Tag.Time.ordinal());
      //  time.setTitle(getString(R.string.Time));
      //  time.setDateValue(new Date());
      //  time.setDateFormat(new SimpleDateFormat("hh:mm a", Locale.US));
      //  elements.add(time.build());

       // DateTimeBuilder dateTime = new DateTimeBuilder(Tag.DateTime.ordinal());
       // dateTime.setTitle(getString(R.string.DateTime));
       // dateTime.setDateValue(new Date());
       // dateTime.setDateFormat(new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US));
       // elements.add(dateTime.build());
    }

    //private void addPickers(List<BaseFormElement<?>> elements) {
        //elements.add(new HeaderBuilder(getString(R.string.PreferredItems)).build());

       // DropDownBuilder<ListItem> dropDown = new DropDownBuilder<>(Tag.SingleItem.ordinal());
        //dropDown.setTitle(getString(R.string.SingleItem));
        //dropDown.setDialogTitle(getString(R.string.SingleItem));
       // dropDown.setOptions(fruits);
        //dropDown.setValue(new ListItem(1L, "Banana"));
       // elements.add(dropDown.build());

       // MultiCheckBoxBuilder<ListItem> multiCheckBox = new MultiCheckBoxBuilder<>(Tag.MultiItems.ordinal());
      //  multiCheckBox.setTitle(getString(R.string.MultiItems));
       // multiCheckBox.setDialogTitle(getString(R.string.MultiItems));
       // multiCheckBox.setOptions(fruits);
      //  multiCheckBox.setOptionsSelected(Collections.singletonList(new ListItem(1L, "Banana")));
       // elements.add(multiCheckBox.build());
   // }

   // private void addAutoComplete(List<BaseFormElement<?>> elements) {
       // AutoCompleteBuilder<ContactItem> autoComplete = new AutoCompleteBuilder<>(Tag.AutoCompleteElement.ordinal());
       // autoComplete.setTitle(getString(R.string.AutoComplete));
        //autoComplete.setArrayAdapter(new ContactAutoCompleteAdapter(this,
               // android.R.layout.simple_list_item_1,
               // new ArrayList<>(Collections.singletonList(new ContactItem(1L, "", "Try \"Apple May\"")))));
       // autoComplete.setDropdownWidth(ViewGroup.LayoutParams.MATCH_PARENT);
       // autoComplete.setValue(new ContactItem(1L, "John Smith", "John Smith (Tester)"));
       // elements.add(autoComplete.build());

      //  AutoCompleteTokenBuilder<ContactItem> autoCompleteToken = new AutoCompleteTokenBuilder<>(Tag.AutoCompleteTokenElement.ordinal());
       // autoCompleteToken.setTitle(getString(R.string.AutoCompleteToken));
      //  autoCompleteToken.setArrayAdapter(new EmailAutoCompleteAdapter(this,
              //  android.R.layout.simple_list_item_1));
       // autoCompleteToken.setDropdownWidth(ViewGroup.LayoutParams.MATCH_PARENT);
      //  autoCompleteToken.setHint("Try \"Apple May\"");
      //  elements.add(autoCompleteToken.build());


      //  TextViewBuilder textView = new TextViewBuilder(Tag.TextViewElement.ordinal());
     //   textView.setTitle(getString(R.string.TextView));
      //  textView.setValue("This is readonly!");
      //  elements.add(textView.build());

    //}




    @Override
    public void onValueChanged(@NotNull BaseFormElement<?> formElement) {
        Toast.makeText(this,
                (formElement.getValue() != null) ? formElement.getValue().toString() :
                        (formElement.getOptionsSelected() != null) ?
                                formElement.getOptionsSelected().toString() : "",
                Toast.LENGTH_SHORT).show();
    }

    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


}
