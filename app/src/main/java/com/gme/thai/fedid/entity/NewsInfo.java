package com.gme.thai.fedid.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuchichi on 2018/11/14.
 */

public class NewsInfo  extends BaseEntity{


    /**
     * data : {"records":[{"id":"1","associationid":"001","title":"Thai-China trade pact signed ","thumbnail":"https://pbs.twimg.com/profile_images/973566406733254657/B_dAPT7w_400x400.jpg","img":"https://www.bangkokpost.com/media/content/20181108/c1_1572030_181108041648_620x413.jpg","description":"<p>Beijing: Thailand is moving ahead with plans to upgrade economic cooperation with China, agreeing Wednesday to the comprehensive framework on enhancing trade and economic partnership between the two nations and aiming to double bilateral trade to US$140 billion (4.59 trillion baht) by 2021.<\/p>","link":"Bangkok Post","createby":"admin","createdt":"13-11-2018 11:00:00"},{"id":"2","associationid":"001","title":"Thai-China trade pact signed 2","thumbnail":"https://pbs.twimg.com/profile_images/973566406733254657/B_dAPT7w_400x400.jpg","img":"https://www.bangkokpost.com/media/content/20181108/c1_1572030_181108041648_620x413.jpg","description":"<p>Beijing: Thailand is moving ahead with plans to upgrade economic cooperation with China, agreeing Wednesday to the comprehensive framework on enhancing trade and economic partnership between the two nations and aiming to double bilateral trade to US$140 billion (4.59 trillion baht) by 2021.<\/p>","link":"Bangkok Post","createby":"admin","createdt":"13-11-2018 11:00:00"},{"id":"3","associationid":"001","title":"Thai-China trade pact signed 3","thumbnail":"https://pbs.twimg.com/profile_images/973566406733254657/B_dAPT7w_400x400.jpg","img":"https://www.bangkokpost.com/media/content/20181108/c1_1572030_181108041648_620x413.jpg","description":"<p>Beijing: Thailand is moving ahead with plans to upgrade economic cooperation with China, agreeing Wednesday to the comprehensive framework on enhancing trade and economic partnership between the two nations and aiming to double bilateral trade to US$140 billion (4.59 trillion baht) by 2021.<\/p>","link":"Bangkok Post","createby":"admin","createdt":"13-11-2018 11:00:00"}]}
     */

    private DataBean data;


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        private List<RecordsBean> records;

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }


    }
    public static class RecordsBean implements Serializable{
        /**
         * id : 1
         * associationid : 001
         * title : Thai-China trade pact signed
         * thumbnail : https://pbs.twimg.com/profile_images/973566406733254657/B_dAPT7w_400x400.jpg
         * img : https://www.bangkokpost.com/media/content/20181108/c1_1572030_181108041648_620x413.jpg
         * description : <p>Beijing: Thailand is moving ahead with plans to upgrade economic cooperation with China, agreeing Wednesday to the comprehensive framework on enhancing trade and economic partnership between the two nations and aiming to double bilateral trade to US$140 billion (4.59 trillion baht) by 2021.</p>
         * link : Bangkok Post
         * createby : admin
         * createdt : 13-11-2018 11:00:00
         */

        private String id;
        private String associationid;
        private String title;
        private String thumbnail;
        private String img;
        private String description;
        private String link;
        private String createby;
        private String createdt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAssociationid() {
            return associationid;
        }

        public void setAssociationid(String associationid) {
            this.associationid = associationid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getCreateby() {
            return createby;
        }

        public void setCreateby(String createby) {
            this.createby = createby;
        }

        public String getCreatedt() {
            return createdt;
        }

        public void setCreatedt(String createdt) {
            this.createdt = createdt;
        }
    }
}
