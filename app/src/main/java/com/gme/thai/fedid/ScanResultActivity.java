package com.gme.thai.fedid;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

public class ScanResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Result Scan");

        Bundle bundle = getIntent().getExtras();
        String value1 = bundle.getString("key1");




        TextView textView4 =(TextView) findViewById(R.id.textView4);

        //textView4.setText(value1);
        textView4.setText(Html.fromHtml(value1));
        textView4.setPaintFlags(textView4.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //textView4.setMovementMethod(LinkMovementMethod.getInstance());

        textView4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                Intent browser= new Intent(Intent.ACTION_VIEW, Uri.parse(value1));
                startActivity(browser);
            }

        });



    }
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }






}
